export const saveToken = token => {
    return {
        type: 'SET_TOKEN',
        token
    }
}

export const clearToken = () => {
    return {
        type: 'CLEAR_TOKEN'
    }
}

export const saveUser = user => {
    return {
        type: 'SET_USER',
        user
    }
}

export const clearUser = () => {
    return {
        type: 'CLEAR_USER'
    }
}
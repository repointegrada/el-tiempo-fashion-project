// React
import React from 'react';
// Background Image
import backgroundImage from "../assets/images/eltiempo.jpeg"
// Component
import CategoryLists from './CategoryLists'
// Material UI
import { Button, withStyles } from '@material-ui/core'
import { Send } from '@material-ui/icons'

const AuthElements = props => {
    // Destructuring the props
    const { user, login } = props

    /**
     * Method that show a button depending the case if user props exist or not exist
     */
    const logInButton = () => {
        if (user) return (
            <CategoryLists />
        )

        return (
            <div className={props.classes.container}>
                <div className={props.classes.logoContainer}>
                    <img src={backgroundImage} alt="background" className={props.classes.image} />
                    <p className={props.classes.text}>Fashion</p>
                </div>
                <Button
                    variant="contained"
                    size="medium"
                    color="primary"
                    startIcon={<Send />}
                    onClick={login}
                >
                    Iniciar sesión con Google
      </Button>
            </div>
        )
    }

    return (
        <>
            {logInButton()}
        </>
    )
}

export default withStyles({
    container: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        alignItems: 'center',
        backgroundColor: '#015f9d',
        paddingLeft: 220,
        paddingRight: 220,
        paddingTop: 0,
        paddingBottom: 0
    },
    logoContainer: {
        padding: 170
    },
    text: {
        display: 'flex',
        justifyContent: 'center',
        fontWeight: 'bold',
        marginTop: 0,
        marginBottom: 0,
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 40
    },
    image: {
        height: 200,
    }
})(AuthElements);
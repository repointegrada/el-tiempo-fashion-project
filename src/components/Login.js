// React
import React, { Component } from 'react'
// Component
import AuthElements from '../components/AuthElements'
// Firebase
import firebaseConf from '../firebase'
import firebase from 'firebase'
// Redux
import { connect } from 'react-redux'
import { saveToken, clearToken } from '../redux/actions'

class Login extends Component {

    /**
     * Method that permit to user to login with auth firebase method
     */
    login = () => {
        let provider = new firebase.auth.GoogleAuthProvider();
        firebaseConf.auth().signInWithPopup(provider).then(result => {
            let token = result.credential.accessToken
            this.props.saveToken(token)
        }).catch(error => {
            console.log(error);
        })
    }

    /**
     * Call to firebase signOut method to log out the user
     */
    logout = () => {
        firebaseConf.auth().signOut().then(() => {
            this.props.clearToken()
        })
    }

    render() {
        return (
            <AuthElements
                login={this.login}
                logout={this.logout}
                token={this.props.token}
                user={this.props.user}
            />
        )
    }
}

const mapsStateToProps = state => {
    return {
        token: state.token,
        user: state.user
    }
}

const mapDispatchToProps = {
    saveToken,
    clearToken
}

export default connect(mapsStateToProps, mapDispatchToProps)(Login)
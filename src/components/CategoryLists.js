// Material ui
import { GridList, GridListTile, GridListTileBar, IconButton, ListSubheader, Toolbar, Typography, withStyles } from '@material-ui/core'
// Icon
import ExitToApp from '@material-ui/icons/ExitToApp'
// React
import React, { Component } from 'react'
// Logo
import backgroundImage from "../assets/images/eltiempo.jpeg"
// Firebase config
import firebaseConf from '../firebase'

class CategoryLists extends Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentWillMount = () => {
        this.writeUserData()
    }

    /**
     * Obtain data of firebase and set state with the garment categories
     */
    writeUserData() {
        const db = firebaseConf.firestore();
        db.collection("garmentCategories")
            .get()
            .then(querySnapshot => {
                const data = querySnapshot.docs.map(doc => doc.data());
                this.setState({
                    data
                })
            });
    }

    /**
     * Method for log out the page
     */
    logout = () => {
        firebaseConf.auth().signOut().then()
    }

    render() {
        // Destructuring
        const { headerTitle, image, root, gridList, title, sectionTitle } = this.props.classes
        return (
            <>
                <Toolbar style={{ backgroundColor: '#015f9d' }}>
                    <Typography variant="h6" noWrap className={headerTitle}>
                        <img src={backgroundImage} alt="background" className={image} />
                    </Typography>
                    <IconButton onClick={this.logout} style={{ color: 'white' }}>
                        <ExitToApp />
                    </IconButton>
                </Toolbar>
                <div className={root}>
                    <GridList cellHeight={200} className={gridList}>
                        <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                            <ListSubheader className={title}>Listado de categorías de prendas </ListSubheader>
                        </GridListTile>
                        {this.state.data.map((tile, index) => (
                            <GridListTile key={index}>
                                <img src={tile.image} alt={tile.image} />
                                <GridListTileBar
                                    title={<span className={sectionTitle}>{tile.garment}</span>}
                                    subtitle={<span>{tile.type}</span>}
                                />
                            </GridListTile>
                        ))}
                    </GridList>
                </div>
            </>
        )
    }
}

export default withStyles({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: 'whitesmoke'
    },
    image: {
        height: 50,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 0,
        paddingLeft: 10
    },
    headerTitle: {
        flexGrow: 1,
    },
    gridList: {
        width: 500,
        height: 'auto',
        overflowY: 'initial'
    },
    title: {
        fontSize: 30,
        fontWeight: 700,
        textTransform: 'uppercase'
    },
    sectionTitle: {
        textTransform: 'uppercase'
    }
})(CategoryLists);

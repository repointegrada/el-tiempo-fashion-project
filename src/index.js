// React
import React from 'react';
import ReactDOM from 'react-dom';
// Redux
import { Provider } from 'react-redux';
import { clearUser, saveUser } from './redux/actions';
import store from './redux/store';
import * as serviceWorker from './serviceWorker';
// Components
import App from './App';
// Config
import firebaseConf from './firebase';
// Style
import './index.css';

firebaseConf.auth().onAuthStateChanged(user => {
  if (user) {
    store.dispatch(saveUser(user))
  } else {
    store.dispatch(clearUser())

  }
})

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
